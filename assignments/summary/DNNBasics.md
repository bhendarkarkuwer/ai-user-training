                                                       Name- Kuwer Bhendarkar @bhendarkarkuwer
                                                       Email - bhendarkarkuwer@gmail.com
                                                       @shunyaos 
**DNN (DEEP NEURAL NETWORK)** 

*Neural Network*

A neural network is a network or circuit of neurons, or in a modern sense, an artificial neural network, composed of artificial neurons or nodes.

*Neurons* : Thing that holds a number between 0 and 1.
![Screenshot_2020-07-21_at_11.31.32_AM](/uploads/5248e4af9db433b9749fec7410242930/Screenshot_2020-07-21_at_11.31.32_AM.png)

Ranging from 0 black pixel to 1 white pixels
![Screenshot_2020-07-21_at_11.33.52_AM](/uploads/8ff84473128494cf26539992720b1d30/Screenshot_2020-07-21_at_11.33.52_AM.png)![Screenshot_2020-07-21_at_11.34.08_AM](/uploads/a5202ce0270a2b3878eb06e4621e8810/Screenshot_2020-07-21_at_11.34.08_AM.png)![Screenshot_2020-07-21_at_11.34.01_AM](/uploads/9276e66f353b2bf909acdba98a527724/Screenshot_2020-07-21_at_11.34.01_AM.png)

The number inside the neuron is called as activation number.
![Screenshot_2020-07-21_at_12.31.55_PM](/uploads/d6975b8602e7e1fef1aa6e7071737a90/Screenshot_2020-07-21_at_12.31.55_PM.png)

There are such 28*28 neuron grids = 784
![Screenshot_2020-07-21_at_12.38.06_PM](/uploads/c671e17f0a6a7ece291b045d7ade767c/Screenshot_2020-07-21_at_12.38.06_PM.png)

This 784 makes the first layer
![Screenshot_2020-07-21_at_12.39.01_PM](/uploads/9f02112a34017faf4a5e6d6ce5113905/Screenshot_2020-07-21_at_12.39.01_PM.png)

Activation in first layer correspond some activation in hidden layers to reach upto the final layer and predict the final output number 
![Screenshot_2020-07-21_at_12.42.07_PM](/uploads/09b8daaf551a10da3ed7ad41ff439247/Screenshot_2020-07-21_at_12.42.07_PM.png)

layers are formed by getting a genral pattern or idea for eg-
![Screenshot_2020-07-21_at_12.44.24_PM](/uploads/90935b208b1dae13910b803742a54d51/Screenshot_2020-07-21_at_12.44.24_PM.png)

EDGE DETECTION - assigning wieghts to different connection from first layer to next .
![Screenshot_2020-07-21_at_2.23.35_PM](/uploads/698b260f2aff4f4775be314b9ae9aaae/Screenshot_2020-07-21_at_2.23.35_PM.png)
These wieghts and activation number together sum up to give result
![Screenshot_2020-07-21_at_2.26.27_PM](/uploads/f1796d757d620b379e53d48baa165945/Screenshot_2020-07-21_at_2.26.27_PM.png)

*SIGMOID FUNCTION* - makes the value ranfing between 1 and 0 
![Screenshot_2020-07-21_at_2.30.55_PM](/uploads/0494ab4edcea25f3c64a1b21f30700ff/Screenshot_2020-07-21_at_2.30.55_PM.png)

the Graph is as shown for sigmoid function
![Screenshot_2020-07-21_at_2.31.43_PM](/uploads/4117caf82418efac7860e6ceadf6100e/Screenshot_2020-07-21_at_2.31.43_PM.png)

The bias tells us how hight the weighted sum needs to be before the neuron starts getting meaningfully active
![Screenshot_2020-07-21_at_2.35.02_PM](/uploads/c585799db52f5ba5d3d81e8a19d91693/Screenshot_2020-07-21_at_2.35.02_PM.png)

![Screenshot_2020-07-21_at_2.35.50_PM](/uploads/9c175ec5461fa8b2a54803e38138b6fd/Screenshot_2020-07-21_at_2.35.50_PM.png)

![Screenshot_2020-07-21_at_2.38.38_PM](/uploads/075838d59c2fc4f4218cf110ec285288/Screenshot_2020-07-21_at_2.38.38_PM.png)

*RELU FUNCTION*
fast to learn
![Screenshot_2020-07-21_at_2.42.36_PM](/uploads/4d9aca48ff12f357ea4d652228813640/Screenshot_2020-07-21_at_2.42.36_PM.png)

*COST FUNCTION*

It is a function that measures the performance of a Machine Learning model for given data. Cost Function quantifies the error between predicted values and expected values and presents it in the form of a single real number

![Screenshot_2020-07-21_at_4.20.05_PM](/uploads/9c356cc684c7ddfdb4c8ecdf819632f4/Screenshot_2020-07-21_at_4.20.05_PM.png)

The cost function minimizes the value of the funtion 
![Screenshot_2020-07-21_at_4.21.16_PM](/uploads/9d6d82422f1066fd9bb7d1bfd50f1ced/Screenshot_2020-07-21_at_4.21.16_PM.png)

*GRADIENT DESCENT*
Gradient of the value gives the direction of steepest ascent

![Screenshot_2020-07-21_at_4.29.00_PM](/uploads/488c44d56af112094f1b40775203cddc/Screenshot_2020-07-21_at_4.29.00_PM.png)

*BACK PROPOTION*
Back-propagation is the essence of neural net training. It is the method of fine-tuning the weights of a neural net based on the error rate obtained in the previous epoch (i.e., iteration). Proper tuning of the weights allows you to reduce error rates and to make the model reliable by increasing its generalization.
![Screenshot_2020-07-21_at_4.55.41_PM](/uploads/d45f259eebc4f6438faa7f8a27726d42/Screenshot_2020-07-21_at_4.55.41_PM.png)

*STOCHASTIC GRADIENT DESCENT*
Stochastic gradient descent (often abbreviated SGD) is an iterative method for optimizing an objective function with suitable smoothness properties (e.g. differentiable or subdifferentiable). It can be regarded as a stochastic approximation of gradient descent optimization, since it replaces the actual gradient (calculated from the entire data set) by an estimate thereof (calculated from a randomly selected subset of the data).

![Screenshot_2020-07-21_at_4.59.34_PM](/uploads/b3f3df53be2c885c89add4f44eaa22ea/Screenshot_2020-07-21_at_4.59.34_PM.png)





